# I wrote a Code and a Game class. My Code class represented a
# sequence of four pegs. The Game kept track of how many turns has
# passed, the correct Code, prompt the user for input.
#
# I also wrote a Code::parse(input) method
# that took a user input string like "RGBY" and built a Code object.
# I made code objects for both (1) the secret code and (2) the
# user's guess of the code. I wrote methods like
# Code#exact_matches(other_code) and Code#near_matches(other_code)
# to handle comparisons.

class Code
  attr_reader :pegs

  PEGS = {
    "R" => :red,
    "G" => :green,
    "B" => :blue,
    "P" => :purple,
    "O" => :orange,
    "Y" => :yellow,
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.random
    pegs = []
    4.times {pegs << PEGS.values.sample}
    Code.new(pegs)
  end

  def self.parse(input)
    pegs = input.split("").map do |char|
      raise "Entered invalid color" unless PEGS.include?(char.upcase)
      PEGS[char.upcase]
    end
    Code.new(pegs)
  end

  def [](i)
    pegs[i]
  end

  def exact_matches(second_code)
    matches = 0
    pegs.each_index do |i|
      # p @pegs[i].to_s + "|||" + second_code[i].to_s
      matches += 1 if @pegs[i] == second_code[i]
    end
    return matches
  end

  def count
    count = Hash.new(0)
    @pegs.each do |color|
      count[color] += 1
    end
    count
  end

  def near_matches(second_code)
    near_matches = 0
    second_count = second_code.count

    self.count.each do |color, count|
      if second_count.has_key?(color)
        near_matches += [count, second_count[color]].min
      end
    end
    near_matches - exact_matches(second_code)
  end

  def ==(second_code)
    return false unless second_code.is_a? Code
    return true if @pegs == second_code.pegs
  end


end



class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "Enter a guess"
    Code.parse(gets.chomp)
  end

  def display_matches(get_guess)
    guess = get_guess
    puts "You have #{@secret_code.exact_matches(guess)} exact matches"
    puts "You have #{@secret_code.near_matches(guess)} near matches"
  end

end
